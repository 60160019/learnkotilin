package introduction

fun main(){
//    This takes the infix function from above one step further using the "operator" modifier.
    operator fun Int.times(str:String) = str.repeat(this)
//    The operator symbol for "times()" is "*" so that you can call the function using "2 *"Bye" ".
    println(2 * "Bye")
    println(2*3*"Bekky")
//    An operator function allows easy range access on strings
    operator fun String.get(range:IntRange) = substring(range)
    val str = "Hello from other side"
//    The "get()" operator enables "bracket-access syntax".
    println(str[0..14])

    printAll("Hello","Hallo","Salut","Hola","こにちわ")
    printAllWithPrefix("Hello","Ola","Salut","こにちわ",prefix = "สวัสดี")
}

// functions with "Vararg" parameters

// the vararg modifier turns a parameter into a vararg
fun printAll(vararg messages:String){
    for (m in messages) println(m)
}
// Thanks to named parameters, you can even add another parameter of the same type after the vararg.
// This wouldn't be allowed in Java because there's no way to pass a value.
fun printAllWithPrefix(vararg messages:String,prefix:String){
    for (m in messages) println(prefix+" "+m)
}

// At runtime, a vararg is just an array.
// To pass it along into a vararg parameter, use the special spread operator * that lets you pass in *entries
// (a vararg of String) instead of entries (an Array<String>).
fun log(vararg  entries:String){
    printAll(*entries)
}