package introduction
fun main(){
    println("Hello World!")
}

/* Kotlin versions before 1.3 main function must have parameter "Array<String>"

    fun main(args: Array<String>){
        println("Hello World!")
    }

 */