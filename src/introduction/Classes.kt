package introduction

// The class declaration consists of the class name, the class header (specifying it's type parameters,
// The primary Constructor ect.)
// and the class body, surrounded by curly braces. Both the header the body are optional;
// if the class has no body, curly braces can be omitted.

// Declares a class named Persons without any properties or user-defined constructors.
// A non-parameterized default constructor is created by Kotlin automatically.
class Persons

// Declares a class with two properties: immutable id and mutable email, and a constructor with two parameters id and email
class Infomation(val id:Int,var email:String)
fun main(){
//    Create an instance of the class Persons via the default constructor. Note that there is no new keyword in Kotlin
    val person = Persons()
//    Creates an instance of the class Information using the constructor with two arguments.
    val infomation = Infomation(1,"Ezkiddo@hotmail.com")
//    Accesses the property id.
    println(infomation.id)
//    Updates the value of the property email
    infomation.email = "useableMail@gmail.com"

}