package introduction

fun main(){
//    Declares a non-null String variable
    var neverNull:String = "Thi can't be null"
//    When trying to assign null to non-nullable variable, a conpilation error is produced.
//    neverNull = null
//    Declares a nullable String variable
    var nullable:String? = "you can keep a null here"
//    Sets the null value to the nullable variable. This is OK.
    nullable = null
//    When inferring types,the compiler assumes non-null for variables that are initialized with a value.
//    เมื่อประกาศตัวแปรโดยไม่ได้ประกาศประเภท คอมไพเลอร์ จะมองว่า ค่าแบบ non-null สำหรับ ตัวแปรที่กำหนดค่า
    var inferredNonNull = "The compiler assumes non-null"
//    When trying to assign null to a variable with inferred type, a compilation error is produced.
//    inferredNonNull = null

//    Declares a function with a non-null string parameter.
    fun strLength(notNull:String):Int{
        return notNull.length
    }
//    Calls the function with a String (non-nullable) argument. This is OK.
    strLength(neverNull)
//    When calling the function with a String? (nullable) argument, a compilation error is produced.
//    strLength(nullable)
}