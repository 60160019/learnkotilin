package introduction

fun main(){
//    Declares a mutable variable and initializing it
    var a:String ="initial"
//    Declares an immutable variable and initializing it.
    var b:Int = 1
//    Declares an immutable variable and initializing it without specifying the type.
//    The compiler infers the type "Int".
    var c = 3
    var d = "Hello"
    var e = false
    println("$a\n$b\n$c\n$d\n$e")

//    Declares a variable without initialization
//    An attempt to use the variable causes a compiler error: "Variable 'f' must be initialized".
//    var f:Int
//    You're free to choose when you initialize a variable, however, it must be initialized before the first read.
//     println(f)
//    Declares a variable without initialization.
//    var i :Int
//    if(doSomething()){
//    Initializes the variable with different values depending on some condition.
//        i = 1
//    }else{
//    Reading the variable is possible because it's already been initialized.
//        i = 2
//    }

}