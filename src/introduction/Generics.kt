package introduction


// Generics are a genericty mechanism that's become standard in modern languages.
// Generic classes and functions increase code reusability by encapsulating common logic that is independent if a particular
// generic type, like the logic inside a List<T> is independent of what T is.

// Note that the implementation makes heavy use of Kotlin's shorthand syntax for functions that can be defined in a single
// expression.


// Defines a generic class MultableStack<E> where E is called the generic type parameter.
// At use-site, it is assigned to a specofoc type such as Int by declaring a "MutableStack<Int>"
class MutablesStack<E>(vararg items:E){
    private val elements = items.toMutableList()
//    Inside the generic class, E can be used as a parameter like any other type.
    fun push(element: E) = elements.add(element)
//    You can also use E as a return type
    fun peek(): E = elements.last()
    fun pop():E = elements.removeAt(elements.size -1)
    fun isEmpty() = elements.isEmpty()
    fun size() = elements.size
    override fun toString() = "MultableStack(${elements.joinToString()}}"
}

// You can also generify functions if their logic is independent of a specific type.
// For instance,you can write a utility function to create mutable stacks.
fun <E> mutableStackOf(vararg ele:E) = MutablesStack(*ele)

// Note that the compiler can infer the generic type from the parameters of "mutableStackOf"
// so that you don't have to write mutableStackOf<Double>()
fun genericFunctions(){
    val stack = mutableStackOf(1,2,3)
    println(stack)
}



fun main (){
    val testMutableStack = MutablesStack("Hello","Suriya","Kenma","Student","Informatics")
    println("Last ${testMutableStack.peek()}")
    testMutableStack.push("Good")
    println("before : $testMutableStack")
    testMutableStack.pop()
    testMutableStack.isEmpty()
    testMutableStack.toString()
    println("After ${testMutableStack.peek()}")
    testMutableStack.push("New")
    println("New : $testMutableStack")
    genericFunctions()
}
