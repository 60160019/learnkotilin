package introduction

fun main(){
//   Defines an infix extension function on Int.
    infix fun Int.items(str:String) = str.repeat(this)
//    Call the infix function
    println(2 items "Bye" )

//    Create a Pair by calling the infix function to from the standard library.
    val pair = "Ferrari" to "Katarina"
    println(pair)

    val testPair = "Suriya" to "Kenma" to "Age" to "22" to "BUU"
    println(testPair)

//    Here's your own implementation of to creatively called onto.
    infix fun String.onto(other:String) = Pair(this,other)
    val myPair = "McLaren" onto "Lucas"
    println(myPair)

    infix fun String.isPaired(other:String) = Pair(other,this)
    val testmyPair = "Lucky" isPaired  "Man"
    println(testmyPair)

    infix fun Int.isDifferance(other: Int) = compareTo(other)
    val testDiff = 1 isDifferance 5
    println(testDiff)

    val sophia = Person("Sophia")
    val claudia = Person("Claudia")
//    Infix notation also works on members functions (methods)
    sophia likes claudia
}

class Person(val name:String){
    private val likedPeople = mutableListOf<Person>()
//  The containing class becomes the first parameter
    infix fun likes(other:Person) {likedPeople.add(other)}
}